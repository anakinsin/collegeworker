from django.db import models
from app.CollegeWorker import models

# Create your models here.
class User:
    id = models.AutoField(_("User ID"), primary_key = True)
    name = models.CharField(_("Name of the Student"), max_length=50)
    roll = models.IntegerField(_("Roll Number"))
    birthdate = models.DateField(_("Date of Birth"), auto_now=False, auto_now_add=False)