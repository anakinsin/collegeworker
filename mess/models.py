from django.db import models
from app.CollegeWorker.models import User

# Create your models here.
class Registration:
    MEALS = ((B, "Breakfast"), (L, "Lunch"), (S, "Snacks"), (D, "Dinner"), )
    MESSES = ((K, "Kadamb"), (N, "North"), (S, "South"), (Y, "Yuktahar"), )
    name = models.ForeignKey("CollegeWorker.User", verbose_name=_("Student Registered"), on_delete=models.CASCADE)
    date = models.DateField(_("Registration Date"), auto_now=False, auto_now_add=False)
    meal = models.CharField(_("Meal Registered form"), choices = MEALS, max_length=1)
    mess = models.CharField(_("Mess Registered in"), choices = MESSES, max_length=1)